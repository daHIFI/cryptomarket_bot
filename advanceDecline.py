import requests, time
def AdvanceDecline(limit):

    payload = {'limit' : limit}
    r = requests.get('https://api.coinmarketcap.com/v1/ticker/', params = payload).json()
    ad_1h = 0
    ad_24h = 0
    ad_7d = 0
    newCoins = []

    for coin in r:
      if (coin['percent_change_1h'] is None) or (coin['percent_change_24h'] is None) or (coin['percent_change_7d'] is None):
        #newCoins.append(str(coin['name']))
        continue
      if float(coin['percent_change_1h']) > 0:
        ad_1h += 1
      else:
        ad_1h -= 1
      if float(coin['percent_change_24h']) > 0:
        ad_24h += 1
      else:
        ad_24h -= 1
      if float(coin['percent_change_7d']) > 0:
        ad_7d += 1
      else:
        ad_7d -= 1
    r = []
    #r.append("The Advance/Decline for the top " + str(limit) + " #crypto coins on @CoinMKTCap at " + time.strftime("%c") + ":")
    r.append("The Advance/Decline for the top " + str(limit) + " #crypto coins on CoinMKTCap:")
    r.append("1h = " + str(ad_1h))
    r.append("24h = " + str(ad_24h))
    r.append("7d = " + str(ad_7d))
    #print ("newcoins length " + len(newCoins))
    if len(newCoins) > 0:
        r.append("New Coins:" + str(newCoins))
    result = "\n".join(r)
    return result
