import sys
import time
import os
import tweepy
import advanceDecline
from dotenv import load_dotenv

load_dotenv()


CONSUMER_KEY = os.getenv("CONSUMER_KEY")
CONSUMER_SECRET = os.getenv("CONSUMER_SECRET")
ACCESS_KEY = os.getenv("ACCESS_KEY")
ACCESS_SECRET = os.getenv("ACCESS_SECRET")
auth = tweepy.OAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)
auth.set_access_token(ACCESS_KEY, ACCESS_SECRET)
api = tweepy.API(auth)


if __name__ == "__main__":

    nums = [10, 100, 1000]
    while True:
        for num in nums:
            tweet = advanceDecline.AdvanceDecline(num)
            try:
                api.update_status(tweet)
                print ('Update at ' + time.strftime("%c"))
            except tweepy.error.TweepError as err:
                print(err.args)
            time.sleep(1800) # Tweet every 30 minutes
